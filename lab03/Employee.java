public class Employee extends Person {
  private String id;

  public Employee(String nm, String gd) {
    super(nm, gd);
    id = "";
  }

  public Employee(String nm, String gd, String id_arg) {
    super(nm, gd);
    id = id_arg;
  }

  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return "Reporting in: " + getName() + " - " + getId();
  }

}
