public abstract class Person {
  private String name;
  private String gender;

  public Person(String nm, String gd) {
    name = nm;
    gender = gd; 
  }

  public String getName() {
    return name;
  }

  public String getGender() {
    return gender;
  }

  public abstract String toString();
}
