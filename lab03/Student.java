public class Student extends Person {
  private SubjectList subs = new SubjectList();

  public Student(String nm, String gd) {
    super(nm, gd);
  }

  public void addSubject(String code, char grade) {
    subs.addSubject(code, grade);
  }

  public void remSubject(String code) {
    subs.remSubject(code);
  }

  public void printTranscript() {
    System.out.println(getTranscript());
  }

  @Override
  public String toString() {
    return "Name: " + getName() + "(" + getGender() + ")";
  }

  private String getTranscript() {
    String sub_results = "";

    for (int i = 0; i < subs.size(); i++) {
      sub_results += subs.get(i).toString() + "\n";
    }

    return toString() + "\n" + sub_results;
  }
}
