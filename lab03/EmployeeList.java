import java.util.*;

public class EmployeeList {
  public static void main(String[] args) {
    Employee s1, s2, s3; 

    ArrayList<Employee> subs = new ArrayList<Employee>();
    for(int i = 0; i < 5;)
      subs.add(new Employee("ABC000" + ++i, "A"));

    for(int i = 0; i < 3;)
      subs.add(new Employee("XYZ000" + ++i, "M", "ID00" + i));
    
    for(int i = 0; i < subs.size(); i++)
      System.out.println(subs.get(i));
  }
}
