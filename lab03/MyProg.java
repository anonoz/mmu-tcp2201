import java.util.*;

public class MyProg {
  static Student anonoz = new Student("Anonoz", "M");
  static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    mainLoop();
    System.out.println("Buh bye!");
  }

  private static void mainLoop() {

  	int menu_option;

    while (true) {
      printMenu();
      menu_option = input.nextInt();

      switch (menu_option) {
        case 1:
          requestNewSubject();
          break;

        case 2:
          requestRemoveSubject();
          break;

        case 3:
          break;

        case 4:
          return;

        default:
          showInputError();
          break;
      }
    }
  }
  
  private static void requestNewSubject() {
  	String subject_code;
  	char subject_grade;

    System.out.println("Enter new subject code: ");
    subject_code = input.next();

    System.out.println("Enter result obtained: ");
    subject_grade = input.nextByte();

    anonoz.addSubject(subject_code, subject_grade);
  }

  private static void requestRemoveSubject() {

  }

  private static void showSubjects() {

  }

  private static void showInputError() {
    System.out.println("I don't get you? ");
  }

  private static void printMenu() {
    System.out.print(
      "Choose an option to continue: \n" +
      "[1] Enter new subject   [2] Remove subject\n" +
      "[3] Show results        [4] Quit\n" +
      "-> "
    );
  }
}