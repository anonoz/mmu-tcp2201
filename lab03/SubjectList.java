import java.util.*;

public class SubjectList extends ArrayList<Subject> {
  public void addSubject(String code, char grade) {
    add(new Subject(code, grade));
  }

  public void remSubject(String code) {
    for (int i = 0; i < size(); i++) {
      if (get(i).code == code) remove(i);
    }
  }
}
