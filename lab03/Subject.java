public class Subject {
  String code;
  char result;

  Subject(String code, char result) {
    this.code = code;
    this.result = result;
  }

  public String toString() {
    return("Subject: " + code + " - " + result);
  }
}
