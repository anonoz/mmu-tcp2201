import java.util.Scanner;

public class exer1 {
  public static void main(String[] args) {
    System.out.print("Enter a number: ");

    Scanner input = new Scanner(System.in);
    int height = input.nextInt();

    // Check range
    if (height < 1 || height > 20) {
      System.out.println("Wrong number range");
      return;
    }

    drawTriangle(height);
  }

  private static void drawTriangle(int height) {
    int level = 1;

    for (; level <= height; level++) {
      for (int column = 0; column < level; column++) {
        System.out.print("* ");
      }

      System.out.print("\n");
    }
  }


}
