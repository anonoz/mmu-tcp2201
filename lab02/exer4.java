import java.util.Scanner;

public class exer4 {
  public static void main(String[] args) {
  	Scanner input = new Scanner(System.in);
  	int size;

    while (true) {
      System.out.print("Enter a number: ");

      size = input.nextInt();

      // Check range
      if (size < 1 || size > 20) {
        System.out.println("Wrong number range");
        break;
      }

      drawSquare(size);
    }
  }

  private static void drawSquare(int size) {
    int column_value;

    for (int row = 1; row <= size; row++) {

      for (int column = 0; column < size; column++) {
        // column_value = (row - 1) * size + column;
        column_value = row + (column * size);
        System.out.printf(" %3d", column_value);
      }

      System.out.print("\n");
    }
  }
}

