import java.util.Scanner;

public class exer3 {
  public static void main(String[] args) {
    System.out.print("Enter a number: ");

    Scanner input = new Scanner(System.in);
    int height = input.nextInt();

    // Check range
    if (height < 1 || height > 20) {
      System.out.println("Wrong number range");
      return;
    }

    // Check odd/even
    if (height % 2 == 0) {
      System.out.println("Input should be odd");
      return;
    }

    drawActualTriangle(height);
  }

  private static void drawActualTriangle(int height) {
    int baseWidth = height;
    int level = 1;

    for (; level <= height; level++) {
      // Pad left
      for (int pad_columns = baseWidth - level; pad_columns > 0; pad_columns--)
        System.out.print(" ");

      for (int column = 0; column < level; column++) {
        System.out.print("* ");
      }

      System.out.print("\n");
    }
  }


}
