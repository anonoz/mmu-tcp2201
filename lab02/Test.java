public class Test {
  public static void main(String[] args) {
    int x = 10;

    while (x < 20) {
      System.out.print("Value of X: " + x);
      x++;
      System.out.print("\n");
    }
  }
}
