import java.util.Scanner;

public class exer2 {
  public static void main(String[] args) {
    System.out.print("Enter a number: ");

    Scanner input = new Scanner(System.in);
    int height = input.nextInt();

    // Check range
    if (height < 1 || height > 20) {
      System.out.println("Wrong number range");
      return;
    }

    drawInvertedTriangle(height);
  }

  private static void drawInvertedTriangle(int height) {
    int level = height;

    for (; level > 0; level--) {
      for (int column = 0; column < level; column++) {
        System.out.print("* ");
      }

      System.out.print("\n");
    }
  }


}
