class argProg {
  public static void main(String[] a){
    if (a.length == 0)
      System.out.println("Welcome to Java.");
    else {
      System.out.println("Length of argument (" + a.length + ") and they are: ");
      for (int i = 0; i < a.length; i++)
        System.out.print(a[i] + " ");
    }
  }
}
