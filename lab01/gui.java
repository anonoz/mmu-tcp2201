import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class gui extends JFrame {

  public gui(String title) {
    super(title);
    setSize(300, 100);
    Container content = getContentPane();
    content.setBackground(Color.white);
    content.setLayout(new FlowLayout());
    content.add(new JLabel("<html>Hello world!<br>WTF man</html>"));
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }
  
  public static void main (String args[]){
    JFrame nframe = new gui("This is a GUI application");
    nframe.setVisible(true);
  }

}
