import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Game extends JFrame implements ActionListener {
  private JPanel jp = new JPanel(new FlowLayout());
  private JPanel boardCards [] = new JPanel[9];
  private String cardNames [] = {"buttonCard", "xCard", "oCard"};
  private JButton btn [] = new JButton[9];
  private int gameboard [] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  private boolean currentPlayerIsX = true;
  
  public Game() {
    super("Tic tac toe");
    
    // Render the buttons
    for (int i = 0; i < boardCards.length; i++) {
      boardCards[i] = makeCardsFor(i);
      jp.add(boardCards[i]);
    }

    add(jp);
    setSize(300, 300);
    setVisible(true);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  public void actionPerformed(ActionEvent e) {
    JButton pressedButton = (JButton)e.getSource();
    String pressedBtnName = pressedButton.getName();
    setMove(pressedButton, Integer.parseInt(pressedBtnName));
  }

  public static void main(String[] args) {
    new Game();
  }

  private JPanel makeCardsFor(int index) {
    JPanel cards = new JPanel(new CardLayout());
    JPanel buttonCard = new JPanel();
    JPanel xCard = new JPanel();
    JPanel oCard = new JPanel();

    // Making button
    JButton btn = new JButton();
    btn.setPreferredSize(new Dimension(75, 75));
    btn.addActionListener(this);
    btn.setName(String.valueOf(index));

    buttonCard.add(btn);

    // Making player cards
    JLabel xLabel = new JLabel("X");
    xLabel.setPreferredSize(new Dimension(75,75));
    xLabel.setHorizontalAlignment(SwingConstants.CENTER);
    xLabel.setVerticalTextPosition(SwingConstants.CENTER);
    xCard.add(xLabel);

    JLabel oLabel = new JLabel("O");
    oLabel.setPreferredSize(new Dimension(75,75));
    oLabel.setHorizontalAlignment(SwingConstants.CENTER);
    oLabel.setVerticalTextPosition(SwingConstants.CENTER);
    oCard.add(oLabel);

    // Compile it
    cards.add(buttonCard, "buttonCard");
    cards.add(xCard, "xCard");
    cards.add(oCard, "oCard");

    return cards;
  }

  private boolean setMove(JButton pressedButton, int position) {
    int player = currentPlayerIsX ? 1 : 2;

    if (gameboard[position] == 0) {
      gameboard[position] = player;
      replaceButtonWithLabel(position, player);
      currentPlayerIsX = !currentPlayerIsX;
      return true;
    } else {
      return false;
    }
  }

  private void replaceButtonWithLabel(int position, int player) {
    ((CardLayout)boardCards[position].getLayout()).show(boardCards[position], cardNames[player]);
  }
}
