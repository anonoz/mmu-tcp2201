import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Something extends JFrame implements ActionListener {
  public Something() {
    super("This is my application");
    JPanel jp = new JPanel(new FlowLayout());
    
    JButton aButton = new JButton("Push me");
    aButton.addActionListener(this);
    aButton.setPreferredSize(new Dimension(200,200));
    jp.add(aButton);

    String[] btnname = {"Open", "Close", "Start", "Stop"};
    for (String s:btnname) {
      JButton btn = new JButton(s);
      btn.addActionListener(this);
      jp.add(btn);
    }

    add(jp);
    setSize(400, 400);
    setVisible(true);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  public void actionPerformed(ActionEvent e) {
    JOptionPane.showMessageDialog(null, "hahaha");
  }

  public static void main(String[] args) {
    new Something();
  }
}
